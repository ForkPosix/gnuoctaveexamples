function cx=cosh(x);
    cx=1;terim=1;f=1;i=0
    while terim>10^-5
        i=i+2
        f=f*i*(i-1);
        terim=x^i/f
        cx=cx+terim;
    end
end