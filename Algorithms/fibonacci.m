function x=fibonacci(N);
    x(1)=1; x(2)=1;
    for i=3:1:N
     x(i)=x(i-1)+x(i-2)
    end
end